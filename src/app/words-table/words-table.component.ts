import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from '../http-service.service';

@Component({
  selector: 'app-words-table',
  templateUrl: './words-table.component.html',
  styleUrls: ['./words-table.component.scss']
})
export class WordsTableComponent implements OnInit {

  constructor(public http: HttpServiceService) { }

  ngOnInit(): void {
  }

  displayedColumns: string[] = ['position', 'text', 'tag'];
}
