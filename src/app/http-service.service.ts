import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  url = 'https://spacy.frag.jetzt/dep';
  data?: any;

  constructor(private http: HttpClient) { }

  // spacyRequest(text: string, language: string): Observable<any> {
  //   let body = {
  //     "text": text,
  //     "model": language
  //   }
  //   return this.http.post<any>(this.url, JSON.stringify(body));
  // }

  spacyRequest(text: string, language: string): void {
    let body = {
      "text": text,
      "model": language
    }
    this.http.post<any>(this.url, JSON.stringify(body)).subscribe(data => {
      this.data = data;
      console.log(data);
    });
  }
  handleError() {
    console.log('not implemented error');
  }
}

