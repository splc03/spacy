import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { Component, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, NgForm} from '@angular/forms';
import { take } from 'rxjs/operators';
import { HttpServiceService } from '../http-service.service';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent implements OnInit {

  text: string = "";
  language: string = "";
  formControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(private _ngZone: NgZone,
    private http: HttpServiceService) {}

  @ViewChild('autosize')
  autosize!: CdkTextareaAutosize;

  ngOnInit(): void {
  }

  submit():void {
    if (this.text && this.language) {
      this.http.spacyRequest(this.text, this.language);
    }
  }

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosize.resizeToFitContent(true));
  }

}
