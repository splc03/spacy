import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from '../http-service.service';

@Component({
  selector: 'app-arcs-table',
  templateUrl: './arcs-table.component.html',
  styleUrls: ['./arcs-table.component.scss']
})
export class ArcsTableComponent implements OnInit {

  constructor(public http: HttpServiceService) { }

  ngOnInit(): void {
  }

  displayedColumns: string[] = ['position', 'text', 'label', 'dir', 'start', 'end'];
}
