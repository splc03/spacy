import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArcsTableComponent } from './arcs-table.component';

describe('ArcsTableComponent', () => {
  let component: ArcsTableComponent;
  let fixture: ComponentFixture<ArcsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArcsTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArcsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
