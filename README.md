# AngularSpaCy Dokumentation
# Language Picker
![Text Input](/Screenshots/Screenshot6.png?raw=true "Text Input")
# Text Input
![Text Input](/Screenshots/Screenshot1.png?raw=true "Text Input")
# Fittet for mobile use
![Text Input](/Screenshots/Screenshot8.png?raw=true "Text Input")
# Fields are required
![Text Input](/Screenshots/Screenshot7.png?raw=true "Text Input")
# German text
![German text](/Screenshots/Screenshot2.png?raw=true "Optional Title")
# English text
![English text](/Screenshots/Screenshot3.png?raw=true "Optional Title")
# English words table
![English words table](/Screenshots/Screenshot4.png?raw=true "Optional Title")
# English arcs table
![English arcs table](/Screenshots/Screenshot5.png?raw=true "Optional Title")

